# Bitbucket

Vendor: Bitbucket
Homepage: https://bitbucket.org

Product: Bitbucket
Product Page: https://bitbucket.org

## Introduction
We classify Bitbucket into the CI/CD domain since Bitbucket handles the versioning, building, storage and deployment.

"Bitbucket Cloud enables you to build, test, and deploy directly from within Bitbucket."

## Why Integrate
The Bitbucket adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Bitbucket Cloud. With this adapter you have the ability to perform operations such as:

- Create, publish, maintain, update repositories in Bitbucket.
- Get Projects
- Commit
- Create Pull Request
- Post Change"

## Additional Product Documentation
The [API documents for Bitbucket](https://developer.atlassian.com/cloud/bitbucket/rest/intro)