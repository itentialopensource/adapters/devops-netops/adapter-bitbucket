
## 0.6.4 [10-15-2024]

* Changes made at 2024.10.14_21:19PM

See merge request itentialopensource/adapters/adapter-bitbucket!25

---

## 0.6.3 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-bitbucket!23

---

## 0.6.2 [08-14-2024]

* Changes made at 2024.08.14_19:36PM

See merge request itentialopensource/adapters/adapter-bitbucket!22

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_21:39PM

See merge request itentialopensource/adapters/adapter-bitbucket!21

---

## 0.6.0 [05-08-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!20

---

## 0.5.7 [03-26-2024]

* Changes made at 2024.03.26_14:27PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!19

---

## 0.5.6 [03-21-2024]

* Changes made at 2024.03.21_14:31PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!18

---

## 0.5.5 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!17

---

## 0.5.4 [03-11-2024]

* Changes made at 2024.03.11_13:24PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!16

---

## 0.5.3 [02-26-2024]

* Changes made at 2024.02.26_13:49PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!15

---

## 0.5.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!14

---

## 0.5.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!13

---

## 0.5.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!12

---

## 0.4.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!10

---

## 0.3.7 [04-18-2022]

- Fix 2 calls
- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!9

---

## 0.3.6 [03-12-2022]

- Updated sample properties

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!7

---

## 0.3.5 [03-12-2022]

- Added commit task

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!8

---

## 0.3.4 [03-02-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!6

---

## 0.3.3 [07-07-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!5

---

## 0.3.2 [01-02-2020]

- Bring the adapter up to the latest adapter foundation

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!4

---

## 0.3.1 [11-19-2019]

- Update the healthcheck url to one that is working in the lab

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!3

---

## 0.3.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!2

---

## 0.2.0 [09-12-2019]

- Update the adapter foundation to the latest

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket!1

---

## 0.1.1 [08-26-2019]

- Initial Commit

See commit 778c7af

---
